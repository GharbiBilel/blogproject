<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Illuminate\Database\Migrations;
use Illuminate\Database\Schema\Blueprint ;
class CommentMigration extends Migration {
    public function up(){
        
       Shema::create('comment',function(Blueprint $tabe){
           $table->increment('id');
           $table->string('message'); 
           $table->integer('note');
           $table->foreign('post_id')->references('id')->on('post')->onDelete('restrict')->onUpdate('restrict');
           $table->foreign('comment_id')->references('id')->on('comment');
           
       });
               
    }
    public function down(){
        
        Schema::table('comment',function (Blueprint $table)
        { $table->dropForeign('post_id');
         $table->dropForeign('comment_id');
         
        }); 
        Schma::drop('comment');
    }
}
