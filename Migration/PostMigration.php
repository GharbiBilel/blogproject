<?php
namespace Illuminate\Database\Migrations; 
use Illuminate\Support\Facades\Schema ;
use Illuminate\Database\Schema\Blueprint;
class PostMigration  extends Migration {
    public function up(){
        Schema::create('post',function(Blueprint $table){
            $table->incements('id');
            $table->string('titre');
            $table->string('content');
            $table->timestamp();
            $table->integer('user_id');
            $table->foriegn('user_id')
                    ->references('id')
                    ->on('user')
                    ->onDelete('restrict')
                   ->onUpdtae('restrict') ;
                    });
                    }
                    public function down(){
                        Schema::table('post',function(Blueprint $table){
                            $table->dropFoerign('user_id');
                        });
                        Schema::drop('post');
                        
                    }
        
    
}
