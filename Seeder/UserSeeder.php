<?php

use Illuminate\Database\Seeder ;
class UserSeeder extends Seeder{ 
    /*
     * créer 10 utilisateurs 
     * @retrun void
     */
    public function run(){
        DB::tabe('users')->delete();
        for ($i;$i<10;++$i){
            BD::table('users')->insert([
				'name' => 'Nom' . $i,
				'email' => 'email' . $i . '@blop.fr',
				'password' => bcrypt('password' . $i),
				]);
                
        }
        
    }
}
