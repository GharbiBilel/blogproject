<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];  
    /*
     * les posts publieé par l'user
     * @return void
     */
    public function post(){
        return  $this->belongsTo('App\Post');
    }
}
