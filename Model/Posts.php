<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model {

	protected $filiable =['titre','contenu']; 
        /*
         * commentaires de post 
         * @return void
         */
	public function comments()
	{
        return $this->hasMany('App\Comments');
        
        }
	/*
         * user qui  commente 
         * @return void 
         */
       
	public function user()
	{
		return $this->belongsTo('App\User');
	}

}
